package com.example.demo;

import com.example.demo.controllers.MainController;
import com.example.demo.model.Pin;
import com.example.demo.repository.PinRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SoftwarePinProjectTests {
    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Autowired
    private WebApplicationContext context;


    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    PinRepository pinRepository;

    @InjectMocks
    MainController mainController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();
    }

    @Test
    public void add() throws Exception {
        Pin savedPin = new Pin();
        savedPin.setCreate_user("createUser");
        savedPin.setAccount("account");
        savedPin.setPin("2512309");
        savedPin.setExpire_timestamp(LocalDateTime.MAX);
        when(pinRepository.save(any(Pin.class))).thenReturn(savedPin);
        when(pinRepository.findByPin(any(String.class))).thenReturn(Optional.empty());

        Pin pin = new Pin();
        pin.setCreate_user("createUser");
        pin.setAccount("account");
        this.mockMvc.perform(post("/pin/new")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(pin)))
                .andExpect(status().isOk())
                .andDo(document("Create PIN"));
    }

    @Test
    public void claim() throws Exception {
        Pin oldPin = new Pin();
        oldPin.setCreate_user("createUser");
        oldPin.setAccount("account");
        oldPin.setPin("2512309");
        oldPin.setExpire_timestamp(LocalDateTime.MAX);

        when(pinRepository.findByPin(any(String.class))).thenReturn(Optional.of(oldPin));

        Pin pin = new Pin();
        pin.setClaim_user("claimUser");
        pin.setAccount("account");
        pin.setPin("2512309");
        this.mockMvc.perform(post("/pin/claim")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.objectMapper.writeValueAsString(pin)))
                .andExpect(status().isOk())
                .andDo(document("Claim PIN"));
    }

}
