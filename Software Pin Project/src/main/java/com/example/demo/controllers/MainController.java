package com.example.demo.controllers;

import com.example.demo.model.Pin;
import com.example.demo.repository.PinRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.SecureRandom;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/pin")
public class MainController {

    private final PinRepository pinRepository;

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    public MainController(PinRepository pinRepository) {
        this.pinRepository = pinRepository;
    }

    @PostMapping(value = "/new")
    public Pin add(@RequestBody Pin pin, HttpServletRequest request) {
        logger.info("Request from {} | User: {} | Account: {}",
                request.getRemoteAddr(), pin.getCreate_user(), pin.getAccount());

        if (pin.getCreate_user() == null) {
            throw new IllegalArgumentException("No create user");
        }

        if (pin.getAccount() == null) {
            throw new IllegalArgumentException("No account");
        }


        pin.setCreate_ip(request.getRemoteAddr());
        String tempPin = String.format("%06d", new SecureRandom().nextInt(1000000));

        while (pinRepository.findByPin(tempPin).isPresent()) {
            tempPin = String.format("%06d", new SecureRandom().nextInt(1000000));
        }
        pin.setPin(tempPin + getChecksum(tempPin));
        pin.setCreate_timestamp(LocalDateTime.now());
        pin.setExpire_timestamp(LocalDateTime.now().plusMinutes(30));
        logger.info("PIN created for {} | PIN: {} | Expire Time: {}",
                request.getRemoteAddr(), pin.getPin(), pin.getExpire_timestamp());

        return this.pinRepository.save(pin);
    }


    @PostMapping(value = "/claim")
    public Pin claim(@RequestBody Pin claimPin, HttpServletRequest request) {

        logger.info("Request from {} | PIN: {} | User: {} | Account: {}",
                request.getRemoteAddr(), claimPin.getPin(), claimPin.getClaim_user(), claimPin.getAccount());



        if (claimPin.getClaim_user() == null) {
            throw new IllegalArgumentException("No claim user");
        }

        if (getSum(claimPin.getPin()) % 10 != 0) {
            throw new IllegalArgumentException("Checksum failed");
        }
        //do {
            Pin pin = pinRepository.findByPin(claimPin.getPin())
                    .orElseThrow(() -> new IllegalArgumentException("Pin does not exist"));
            if (pin.getExpire_timestamp().isBefore(LocalDateTime.now())) {
                throw new IllegalArgumentException("Pin expired.");
            }
            if (pin.getClaim_user() != null) {
                throw new IllegalArgumentException("Pin already claimed.");
            }

            pin.setClaim_ip(request.getRemoteAddr());
            pin.setClaim_timestamp(LocalDateTime.now());
            pin.setClaim_user(claimPin.getClaim_user());
            logger.info("PIN successfully claimed.  IP: {} | PIN: {} | User: {} | Account: {}",
                    request.getRemoteAddr(), claimPin.getPin(), claimPin.getClaim_user(), claimPin.getAccount());
        //}while(true);
        return pinRepository.save(pin);
    }

    private String getChecksum(String pin) {
        int sum = getSum(pin);
        sum *= 9;
        return (sum % 10) + "";
    }

    private int getSum(String pin) {
        int[] digits = new int[pin.length()];
        int sum = 0;

        int alternate = 1;
        for (int i = 0; i < pin.length(); i++) {
            digits[i] = Character.getNumericValue(pin.charAt(i));
            digits[i] *= alternate;
            alternate = alternate == 1 ? 2 : 1;

            if (digits[i] > 9) {
                digits[i] = digits[i] - 9;
            }
            sum += digits[i];
        }
        return sum;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    private String handleExceptions(IllegalArgumentException ex) {
        logger.warn("Returned as bad request. {}", ex.getMessage());
        return ex.getMessage();
    }


}
