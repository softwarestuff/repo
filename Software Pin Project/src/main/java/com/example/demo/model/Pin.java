package com.example.demo.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
public class Pin {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer oid;

    @Column(name = "account", nullable = false)
    private String account;

    @Column(name = "pin", nullable = false, unique = true)

    private String pin;

    @Column(name = "create_ip", nullable = false)
    private String create_ip;

    @Column(name = "create_user", nullable = false)
    private String create_user;

    @Column(name = "create_timestamp", updatable = false)
    private LocalDateTime create_timestamp;

    @Column(name = "expire_timestamp")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime expire_timestamp;

    @Column(name = "claim_timestamp")
    private LocalDateTime claim_timestamp;

    @Column(name = "claim_user")
    private String claim_user;

    @Column(name = "claim_ip")
    private String claim_ip;


    public Pin(String account, String pin, String create_ip, String create_user, LocalDateTime create_timestamp, LocalDateTime expire_timestamp, LocalDateTime claim_timestamp, String claim_user, String claim_ip) {
        this.account = account;
        this.pin = pin;
        this.create_ip = create_ip;
        this.create_user = create_user;
        this.create_timestamp = create_timestamp;
        this.expire_timestamp = expire_timestamp;
        this.claim_timestamp = claim_timestamp;
        this.claim_user = claim_user;
        this.claim_ip = claim_ip;
    }

    public Pin() {}

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getCreate_ip() {
        return create_ip;
    }

    public void setCreate_ip(String create_ip) {
        this.create_ip = create_ip;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    public LocalDateTime getCreate_timestamp() {
        return create_timestamp;
    }

    public void setCreate_timestamp(LocalDateTime create_timestamp) {
        this.create_timestamp = create_timestamp;
    }

    public LocalDateTime getExpire_timestamp() {
        return expire_timestamp;
    }

    public void setExpire_timestamp(LocalDateTime expire_timestamp) {
        this.expire_timestamp = expire_timestamp;
    }

    public LocalDateTime getClaim_timestamp() {
        return claim_timestamp;
    }

    public void setClaim_timestamp(LocalDateTime claim_timestamp) {
        this.claim_timestamp = claim_timestamp;
    }

    public String getClaim_user() {
        return claim_user;
    }

    public void setClaim_user(String claim_user) {
        this.claim_user = claim_user;
    }

    public String getClaim_ip() {
        return claim_ip;
    }

    public void setClaim_ip(String claim_ip) {
        this.claim_ip = claim_ip;
    }
}
