package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftwarePinProject {

	public static void main(String[] args) {
		SpringApplication.run(SoftwarePinProject.class, args);
	}
}
